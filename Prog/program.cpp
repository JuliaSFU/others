﻿#include "stdafx.h"
#include <fstream>
#include "program.h"
#include "string"
#include "iostream"
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

namespace all_shapes
{
	void color_out(string color, float density, float perim, ofstream &ofst)
	{
		ofst << ", color = " << color;
		ofst << ", density = " << density;
		ofst << ", perimeter = " << perim << endl;
	}

	float rectangle::shape_perim()
	{
		return 2 * (abs(x1 - x2) + abs(y1 - y2));
	}

	// ¬вод параметров пр€моугольника
	void rectangle::shape_add_data(ifstream &ifst)
	{
		ifst >> x1 >> y1 >> x2 >> y2;
	}

	// ¬ывод параметров пр€моугольника
	void rectangle::shape_out_data(ofstream &ofst) {
		ofst << "It is Rectangle: x1 = " << x1 << ", y1 = " << y1 << ", x2 = " << x2 << ", y2 = " << y2;
	}

	// ¬ывод параметров только пр€моугольника
	void rectangle::rectangle_out_data(ofstream &ofst)
	{
		shape_out_data(ofst);
	}

	float circle::shape_perim()
	{
		return 2 * M_PI * r;
	}

	// ¬вод параметров окружности
	void circle::shape_add_data(ifstream &ifst)
	{
		ifst >> r >> x >> y;
	}

	// ¬ывод параметров окружности
	void circle::shape_out_data(ofstream &ofst) {
		ofst << "It is Circle: r = " << r << ", x = " << x << ", y = " << y;
	}


	float triangle::shape_perim()
	{
		return a + b + c;
	}

	// ¬вод параметров треугольника
	void triangle::shape_add_data(ifstream &ifst)
	{
		ifst >> a >> b >> c;
	}

	// ¬ывод параметров треугольника
	void triangle::shape_out_data(ofstream &ofst) {
		ofst << "It is Triangle: a = " << a << ", b = " << b << ", c = " << c;
	}

	// ¬вод параметров обобщенной фигуры
	shape* shape::shape_in(ifstream &ifst)
	{
		shape *sp;
		int k;
		ifst >> k;
		switch (k)
		{
		case 1:
			sp = new rectangle; 
			break;
		case 2:
			sp = new circle; 
			break;
		case 3:
			sp = new triangle;
			break;
		default:
			return 0;
		}
		sp->shape_add_data(ifst);
		ifst >> sp->color;
		ifst >> sp->density;
		return sp;
	}

	void shape::rectangle_out_data(ofstream &ofst)
	{
		ofst << endl;  
	}

	// ƒеструктор дл€ освобождени€ пам€ти
	void crisper::cont_clear()
	{
		while (length != 0)
		{
			container *tmp = head;
			head = head->next;
			delete tmp;
			length--;
		}
	}

	bool shape::compare(shape &other)
	{
		return shape_perim() < other.shape_perim();

	}

	void crisper::cont_sort()
	{
		container *tmp_head = head; // св€занный список
		container *temp1, *temp2;

		temp1 = tmp_head;
		temp2 = tmp_head;

		for (int in1 = 0; in1 < length; in1++)
		{
			for (int in2 = 0; in2 < length; in2++)
			{
				if (temp1->form->compare(*temp2->form))
				{
					shape *i = temp1->form;
					temp1->form = temp2->form;
					temp2->form = i;
				}
				temp2 = temp2->next;
				if (in2 + 1 == length)
					temp2 = tmp_head;
			}
			temp1 = temp1->next;
			if (in1 + 1 == length)
				temp1 = tmp_head;
		}
	}

	// ƒобавление элемента в список
	void crisper::cont_add(ifstream &ifst)
	{
		while (!ifst.eof()) {
			container *cont = new container;
			cont->form = shape::shape_in(ifst);

			if (length == 0)
				head = tail = cont;
			else
			{
				tail->next = cont;
				cont->prev = tail;
				tail = cont;
			}

			length++;
		}
	}

	// ¬ывод элементов в файл
	void crisper::cont_out(ofstream &ofst)
	{
		ofst << "Container contents " << length
			<< " elements." << endl;

		container *tmp_Head = head;

		int temp = length;
		int i = 0;
		while (temp != 0)
		{
			ofst << i << ": ";
			i++;
			tmp_Head->form->shape_out_data(ofst);
			color_out(tmp_Head->form->color, tmp_Head->form->density, tmp_Head->form->shape_perim(), ofst);
			tmp_Head = tmp_Head->next;
			temp--;
		}
	}

	void crisper::cont_rect_out(ofstream &ofst)
	{
		container *tmp_Head = head;

		int temp = length;
		int i = 0;
		while (temp != 0)
		{
			i++;
			tmp_Head->form->rectangle_out_data(ofst);
			tmp_Head = tmp_Head->next;
			temp--;
		}
	}
}