#include "stdafx.h"
#include <iostream>
#include <fstream>
#include "program.h"

using namespace std;

using namespace all_shapes;

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc != 3) {
		cout << "incorrect command line! "
			"Waited: command infile outfile" << endl;
		exit(1);
	}
	ifstream ifst(argv[1]);
	ofstream ofst(argv[2]);

	cout << "Start" << endl;
	crisper El;

	El.cont_add(ifst);
	ofst << "Filled container." << endl;
	El.cont_out(ofst);

	El.cont_sort();
	ofst << "Sorted container." << endl;
	El.cont_out(ofst);

	ofst << "Only rectangels." << endl;
	El.cont_rect_out(ofst);

	El.cont_clear();
	ofst << "Empty container." << endl;
	El.cont_out(ofst);

	cout << "Stop" << endl;
	return 0;
}

