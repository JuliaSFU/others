#ifndef __program_atd__
#define __program_atd__
#include "string"
#include "iostream"
#include <fstream>

using namespace std;

namespace all_shapes
{
	// ���������, ���������� ��� ������
	class shape
	{
	public:
		static shape* shape_in(ifstream &ifst); // ������������, ���������� � ���� ������ �� ������
		virtual void shape_add_data(ifstream &ifst) = 0; // ����
		virtual void shape_out_data(ofstream &ofst) = 0; // �����
		virtual void rectangle_out_data(ofstream &ofst);
		virtual float shape_perim() = 0; // ���������� ���������
		bool compare(shape &other);
		shape() : color("") {}; //������������ ��������� � ���� � ������� ������������
		string color;
		float density;
	};

	// ���������, ���������� ������������
	class rectangle : public shape
	{
		int x1, y1; // ����������, ����������� ������� ����� ����
		int x2, y2; // ����������, ����������� ������ ������ ����
	public:
		rectangle() : x1(0), y1(0), x2(0), y2(0) {}; //������������ ��������� � ���� � ������� ������������
		~rectangle();
		void shape_add_data(ifstream &ifst); // ����
		void shape_out_data(ofstream &ofst); // �����
		void rectangle_out_data(ofstream &ofst); // ����� �������������
		float shape_perim(); // ���������� ���������
	};

	// ���������, ���������� ����������
	class circle : public shape
	{
		int r; // ������ ����������
		int x, y; // ���������� ������ ����������
	public:
		circle() : r(0), x(0), y(0) {}; //������������ ��������� � ���� � ������� ������������
		~circle();
		void shape_add_data(ifstream &ifst); // ����
		void shape_out_data(ofstream &ofst); // �����
		float shape_perim(); // ���������� ���������
	};

	// ���������, ���������� �����������
	class triangle : public shape
	{
		int a, b, c;
	public:
		triangle() : a(0), b(0), c(0) {}; //������������ ��������� � ���� � ������� ������������
		~triangle();
		void shape_add_data(ifstream &ifst); // ����
		void shape_out_data(ofstream &ofst); // �����
		float shape_perim(); // ���������� ���������
	};

	// ���������, ����������� �� ������ ���������������� ��������� ������
	struct container
	{
		shape *form;
		container *next;
		container *prev;
	};

	//
	class crisper
	{
		container *head, *tail;
		int length;
	public:
		crisper() : head(NULL), tail(NULL), length(0) {};
		~crisper()
		{
			cont_clear();
		};
		void cont_clear();
		void cont_sort();
		void cont_add(ifstream &ifst);
		void cont_out(ofstream &ofst);
		void cont_rect_out(ofstream &ofst);
	};
};
#endif